import redis
import time
import cv2
import numpy as np

def setup(host, port, database, password):
    global r
    r = redis.Redis(host=host, port=port, password=password, db=database)

def getAvailableData(moduleId):
    currentCount = int(r.get(moduleId + "currentCount"))
    highestReachableCount = r.get(moduleId + "highestReachableCount")
    while highestReachableCount is None:
        highestReachableCount = r.get(moduleId + "highestReachableCount")
    highestReachableCount = int(r.get(moduleId + "highestReachableCount"))

    availableData = []

    minCount = currentCount - 1
    dataEncoded = r.get(moduleId + "data" + str(minCount))
    while dataEncoded is not None:
        dataDecoded = dataEncoded.decode("utf-8")
        availableData.append(dataDecoded)
        minCount -= 1
        dataEncoded = r.get(moduleId + "data" + str(minCount))

    minCount = highestReachableCount - 1
    dataEncoded = r.get(moduleId + "data" + str(minCount))
    while dataEncoded is not None and currentCount != highestReachableCount:
        dataDecoded = dataEncoded.decode("utf-8")
        availableData.append(dataDecoded)
        minCount -= 1
        dataEncoded = r.get(moduleId + "data" + str(minCount))

    return availableData

def getCurrentData(moduleId):
    currentCount = int(r.get(moduleId + "currentCount"))
    dataEncoded = r.get(moduleId + "data" + str(currentCount-1))
    while dataEncoded is None:
        currentCount = int(r.get(moduleId + "currentCount"))
        dataEncoded = r.get(moduleId + "data" + str(currentCount - 1))
        print(moduleId + ' probably offline.')
        time.sleep(.5)
    dataDecoded = dataEncoded.decode("utf-8")
    return(dataDecoded)

def getDataImage(moduleId):
    imgEncoded = r.get(moduleId + 'cam')
    imgDecoded = cv2.imdecode(np.frombuffer(imgEncoded, np.uint8), 1)
    return(imgDecoded)