import server
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import MaxNLocator
from datetime import datetime
import numpy as np

# Settings
moduleId = 'moduleA'
fileLocation = './static/media/' + moduleId + '.e41aec95.png'

# Settings server
host = "84.247.9.119"
port = 6379
database = 0
password = '^#FHNM(KG%S@FFF48JG%ghjkvb654#$879&^!!456'

# Setup server
server.setup(host, port, database, password)

while True:
    availableData = server.getAvailableData(moduleId)
    timeList = []
    numberOfObjectsList = []
    numberOfCollisionsList = []
    for dataString in availableData:
        dataList = dataString.split(",")
        time = datetime.strptime(dataList[0], "%Y-%m-%d %H:%M:%S.%f")
        numberOfObjects = int(dataList[1])
        numberOfCollisions = int(dataList[2])
        timeList.append(time)
        numberOfObjectsList.append(numberOfObjects)
        numberOfCollisionsList.append(numberOfCollisions)

    # Plot
    fig = plt.figure(1)
    plt.plot(timeList, numberOfObjectsList, 'bo', markersize=3)
    plt.plot(timeList, numberOfCollisionsList, 'r^', markersize=2.5)

    # Plot layout
    plt.xlabel('Time (hh:mm) \u2192')
    plt.ylabel('Quantity (-) \u2192')

    # Axis
    ax = plt.gca()
    # x
    plt.xticks(rotation=45)
    xFormat = mdates.DateFormatter("%H:%M")
    ax.xaxis.set_major_formatter(xFormat)
    plt.gcf().subplots_adjust(bottom=0.15)
    # y
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))

    # Legend
    plt.legend(('Number of people', 'Number of collisions'), bbox_to_anchor=(0,1.02,1,0.2), loc="lower left",
                mode="expand", borderaxespad=0, ncol=2)

    # Grit
    plt.grid(True)

    # Save plot
    fig.savefig(fileLocation, dpi=fig.dpi)
    fig.clf(1)
