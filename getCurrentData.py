import server
import time
import cv2

# Settings
moduleId = 'moduleA'
fileLocation = './static/media/' + 'dataImage' + '.c08d24a1.png'

# Settings server
host = "84.247.9.119"
port = 6379
database = 0
password = '^#FHNM(KG%S@FFF48JG%ghjkvb654#$879&^!!456'

# Setup server
server.setup(host, port, database, password)

while True:
    # Get number of objects and collisions
    currentDataString = server.getCurrentData(moduleId)
    currentData = currentDataString.split(",")
    currentNumberOfObjects = currentData[1]
    currentNumberOfCollisions = currentData[2]
    with open("./currentData/numberOfObjects.txt", "w") as textFile:
        textFile.write(currentNumberOfObjects)
    with open("./currentData/numberOfCollisions.txt", "w") as textFile:
        textFile.write(currentNumberOfCollisions)

    # Get data image
    img = server.getDataImage(moduleId)
    cv2.imwrite('./static/media/dataImage.c08d24a1.png', img)
    time.sleep(1)
